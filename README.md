# r.stream.culvert
A GRASS GIS module for identifying culverts, bridges and the like in LIDAR terrain models

# Manual
https://invafish.gitlab.io/r.stream.culvert

# Author
Stefan Blumentrath, [Norwegian Institute for Nature Research (NINA)](https://www.nina.no/), Oslo, Norway

Written for the [INVAFISH](https://prosjektbanken.forskningsradet.no/#/project/NFR/243910)
project (RCN MILJ&Oslash;FORSK grant 243910)

#!/usr/bin/env python

"""
MODULE:    r.stream.culvert

AUTHOR(S): Stefan Blumentrath <stefan.blumentrath nina.no>

PURPOSE:   Identify culverts in a LIDAR DEM

COPYRIGHT: (C) 2019 by Norwegian Institute for Nature Research (NINA, Oslo),
           Stefan Blumentrath and the GRASS GIS Development Team

This program is free software under the GNU General Public
License (>=v2). Read the file COPYING that comes with GRASS
for details.
"""

#%module
#% description: Identify culverts, bridges and the like in a LIDAR DEM
#% keyword: raster
#% keyword: stream
#% keyword: culvert
#% keyword: sink
#% keyword: hydrology
#% keyword: lidar
#%end

#%option G_OPT_R_INPUT
#% key: barriers
#% description: Input raster map with barriers like roads and railroad tracks
#%end

#%option G_OPT_R_ELEV
#%end

#%option G_OPT_V_OUTPUT
#% key: culverts
#% description: Resulting vector map with lines to be carved into the DEM
#%end

#%option
#% key: max_outer_dist
#% description: Minimum distance from barries to be considered (in meter)
#% guisection: Settings
#% type: double
#% required: yes
#% answer: 65.0
#%end

#%option
#% key: max_inner_dist
#% description: Minimum distance between sink and channel structure below (in meter)
#% guisection: Settings
#% type: double
#% required: yes
#% answer: 165.0
#%end

#%option
#% key: min_sink_size
#% description: Minimum size of sinks (in number of pixels) to be considered
#% guisection: Settings
#% type: integer
#% required: yes
#% answer: 5
#%end

#%option
#% key: minsize
#% description: Minimum size of channel structures (in number of pixels) to be considered
#% guisection: Settings
#% type: integer
#% required: yes
#% answer: 50
#%end

#%option
#% key: z_tolerance
#% description: Tolerance for differences in altitude (in meter)
#% guisection: Settings
#% type: double
#% required: yes
#% answer: 0.25
#%end

#%option G_OPT_R_INPUT
#% key: filled_dem
#% description: Input elevation raster map with filled sinks (e.g. with r.terraflow)
#% guisection: Optional
#% required: no
#%end

#%option G_OPT_R_INPUT
#% key: channels
#% description: Input raster map with channel structures (e.g. from r.geomorphon)
#% guisection: Optional
#% required: no
#%end

#%option G_OPT_R_INPUT
#% key: streams
#% description: Input raster map streams (e.g. from topographic maps)
#% guisection: Optional
#% required: no
#%end

#%option G_OPT_V_INPUT
#% key: waterbodies
#% description: Input vector map with waterbodies (e.g. from topographic maps)
#% guisection: Optional
#% required: no
#%end

#%option
#% key: memory
#% description: Amount of memory (in MB) to be used for sink filling
#% guisection: Settings
#% type: integer
#% required: no
#% answer: 300
#%end

##%flag
##% key: s
##% description: Smooth borders of channel structures (reduces the number of small atrtifacts)
##% guisection: Settings
##%end


# ToDo:
# add distance to streams
# add distance to culverts
# add distance to streams in sink identification
# Make script DB independent

import os
import atexit
from io import BytesIO
from copy import deepcopy
import sqlite3
import subprocess
import shutil

import numpy as np

import grass.script as gscript
from grass.pygrass import raster

# from grass.script.vector import vector_history

# from grass.pygrass.modules import Module
from grass.pygrass.vector.table import get_path

# Global variables
# global tempname
tempname = gscript.tempname(9)

def cleanup():
    """Remove temporary maps and re-activate user mask"""
    if raster.RasterRow("MASK").exist():
        gscript.run_command("r.mask", flags="r", quiet=True)
    reset_mask()
    gscript.run_command(
        "g.remove",
        type="raster",
        quiet=True,
        pattern="{}*".format(tempname),
        flags="fb",
        stderr=subprocess.DEVNULL,
    )


def unset_mask():
    """Deactivate user mask"""
    if raster.RasterRow("MASK").exist():
        gscript.run_command(
            "g.rename", quiet=True, raster="MASK,{}_MASK".format(tempname)
        )


def reset_mask():
    """Re-activate user mask"""
    if raster.RasterRow("{}_MASK".format(tempname)).exist():
        gscript.run_command(
            "g.rename", quiet=True, raster="{}_MASK,MASK".format(tempname)
        )


def check_db_driver():
    """Check if default DB driver is supported"""
    db_connection = gscript.db.db_connection()["driver"]
    if db_connection == "sqlite":
        sqlite3_version = ".".join(sqlite3.sqlite_version.split(".")[:2])
        if float(sqlite3_version) < 3.25:
            gscript.fatal(
                "Found SQLite3 version {}. At least version 3.25 is required".format(
                    sqlite3_version
                )
            )
    elif db_connection != "pg":
        gscript.fatal("DB driver '{}' not supported.".format(db_connection))

    else:
        gscript.fatal("DB driver '{}' not supported.".format(db_connection))

    return db_connection


def sample_raster_values(points, layer, raster_column_list):
    """
    :param points: The name of the table where indices are created...
    :type points: str
    :param points: Layer to be used for sampling...
    :type points: str
    :param raster_column_list: A list of tuples with pairs of raster map names [0] and column names [1]
    :type raster_column_list: list
    """
    for raster_column in raster_column_list:
        gscript.run_command(
            "v.what.rast",
            quiet=True,
            map=points,
            layer=layer,
            raster=raster_column[0],
            column=raster_column[1],
        )
    return None


def extract_terrain_channels(in_channels, elevation, rcw, minsize, max_outer_dist, streams):
    """Extract morphometric channel network from DEM based on module parameters provided byt the user"""
    # only if no input chanel system is provided
    if not in_channels:
        in_channels = "{}_forms".format(tempname)
        gscript.run_command(
            "r.geomorphon",
            overwrite=True,
            quiet=True,
            elevation=elevation,
            flags="e",
            forms=in_channels,
            search=13,
            skip=5,
            flat=3,
            dist=10
        )

    # Use "hollow (7) as well and as limit for growing algorithm?

    # Remove remove forms of single pixels (probably not required if mode)
    mc_expression = """{tempname}_forms_nsp=if((
    {in_channels} == {in_channels}[0,1] |||
    {in_channels} == {in_channels}[0,-1] |||
    {in_channels} == {in_channels}[1,0] |||
    {in_channels} == {in_channels}[-1,0] |||
    {in_channels} == {in_channels}[1,1] |||
    {in_channels} == {in_channels}[-1,1] |||
    {in_channels} == {in_channels}[1,-1] |||
    {in_channels} == {in_channels}[-1,-1]),
    {in_channels}, mode({in_channels}[0,1],
    {in_channels}[0,-1], {in_channels}[1,0], {in_channels}[-1,0],
    {in_channels}[1,1], {in_channels}[-1,1], {in_channels}[1,-1],
    {in_channels}[-1,-1]))""".format(
        in_channels=in_channels, tempname=tempname
    ).replace(
        "\n", ""
    )
    gscript.run_command("r.mapcalc", quiet=True, expression=mc_expression)

    gscript.write_command(
        "r.reclass",
        overwrite=True,
        quiet=True,
        input="{}_forms_nsp".format(tempname),
        output="{}_forms_neg".format(tempname),
        rules="-",
        stdin="""9 = 1
10 = 1
* = NULL""",
    )
    # downstream structures
    gscript.write_command(
        "r.reclass",
        overwrite=True,
        quiet=True,
        input="{}_barrier_dist".format(tempname),
        output="MASK",
        rules="-",
        stdin="""0 thru {0} = NULL\n* = 1""".format(rcw),
    )

    gscript.run_command(
        "r.clump",
        flags="d",
        overwrite=True,
        quiet=True,
        input="{}_forms_neg".format(tempname),
        output="{}_forms_neg_clump".format(tempname),
        minsize=0,
    )

    gscript.write_command(
        "r.reclass",
        overwrite=True,
        quiet=True,
        input="{}_forms_neg_clump".format(tempname),
        output="{}_forms_neg_clump_size".format(tempname),
        rules="-",
        stdin=gscript.read_command(
            "r.stats",
            flags="nc",
            separator="=",
            input="{}_forms_neg_clump".format(tempname),
            quiet=True,
        ),
    )

    if streams:
        np_arr = np.genfromtxt(
            BytesIO(gscript.read_command(
            "r.univar",
            flags="t",
            separator=" ",
            zones="{}_forms_neg_clump".format(tempname),
            map="{}_stream_dist".format(tempname),
            quiet=True,
        ).replace("  ", " 0 ").encode("UTF8")), dtype=None, names=True
    )

        rel_cats = "\n".join(map(str, np_arr["zone"][np.where(np_arr["min"] < max_outer_dist / 6.0)].tolist()))
        rel_cats += "\n"

        gscript.write_command(
            "r.reclass",
            overwrite=True,
            quiet=True,
            input="{}_forms_neg_clump".format(tempname),
            output="{}_forms_rel_stream_dist".format(tempname),
            rules="-",
            stdin=rel_cats.replace("\n", " = 1\n"),
        )

        # Filter forms by size and distance to streams if provided
        # todo: Replace r.mapcalc with r.reclass
        mc_expression="{0}_forms_rel=if({0}_forms_neg_clump_size>={1}|||{0}_forms_rel_stream_dist,{0}_forms_neg_clump,null())".format(
            tempname, minsize
        )
    else:
        # Filter forms by size and distance to streams if provided
        # todo: Replace r.mapcalc with r.reclass
        mc_expression = "{0}_forms_rel=if({0}_forms_neg_clump_size>={1},{0}_forms_neg_clump,null())".format(
            tempname, minsize
        )

    gscript.run_command(
        "r.mapcalc",
        overwrite=True,
        quiet=True,
        expression=mc_expression)

    gscript.write_command(
        "r.reclass",
        overwrite=True,
        quiet=True,
        input="{}_barrier_dist".format(tempname),
        output="MASK",
        rules="-",
        stdin="""0 thru {0} = NULL\n{1} thru {2} = 1\n* = NULL""".format(
            rcw, rcw + 1, max_outer_dist
        ),
    )

    # Filter forms by elevation difference to barriers
    elev_diff = gscript.read_command(
        "r.univar",
        flags="t",
        quiet=True,
        overwrite=True,
        map="{}_barelevdiff".format(tempname),
        zones="{}_forms_rel".format(tempname),
        separator=" ",
    )

    np_arr = np.genfromtxt(
        BytesIO(elev_diff.replace("  ", " 0 ").encode("UTF8")), dtype=None, names=True
    )
    # Could also further be filtered by maximum elevation difference (drop height)
    # np.where(np_arr['min']<0 & np_arr['min']>-25)
    rel_cats = "\n".join(map(str, np_arr["zone"][np.where(np_arr["min"] < 0)].tolist()))
    rel_cats += "\n"

    gscript.write_command(
        "r.reclass",
        overwrite=True,
        quiet=True,
        input="{}_forms_rel".format(tempname),
        output="{}_forms_rel_diff".format(tempname),
        rules="-",
        stdin=rel_cats.replace("\n", " = 1\n"),
    )

    gscript.run_command("r.mask", quiet=True, overwrite=True, flags="r")
    corner_check = {
        'north_end': ['[0,-1]', '[-1,-1]', '[-1,0]', '[-1,-1]', '[0,1]'],
        'south_end': ['[0,-1]', '[1,-1]', '[1,0]', '[1,1]', '[0,1]'],
        'east_end': ['[-1,0]', '[-1,1]', '[0,1]', '[1,1]', '[1,0]'],
        'west__end': ['[-1,0]', '[-1,-1]', '[0,-1]', '[-1,1]', '[1,0]']
    }

    mc_expression1 = "{0}_forms_rel_min_tmp=if({0}_forms_rel_diff,(\
if({1}>{1}[0,1],1,0)+if({1}>{1}[1,1],1,0)+if({1}>{1}[1,0],1,0)+\
if({1}>{1}[1,-1],1,0)+if({1}>{1}[0,1],1,0)+if({1}>{1}[0,-1])+\
if({1}>{1}[-1,1],1,0)+if({1}>{1}[-1,-1],1,0)+if({1}>{1}[-1,0])),null())".format(tempname, elevation)
    mc_expression2 = "{0}_forms_rel_min=if({0}_forms_rel_min_tmp<2,1, \
if({0}_forms_rel_min_tmp==2||{0}_forms_rel_min_tmp==3, \
if((".format(tempname, elevation)
    for key in corner_check.keys():
        mc_expr_part = '&&'.join(['isnull({{0}}{})'.format(cell) for cell in corner_check[key]])
        mc_expr_part = mc_expr_part.format('{0}_forms_rel_min_tmp'.format(tempname))
        mc_expression2 += '({})'.format(mc_expr_part)
        mc_expression2 += '||'
    mc_expression2 = mc_expression2.rstrip('||')
    mc_expression2 += "),null(),1),null()))".format(tempname, elevation)

    #r.mapcalc --q expression="tmp_qTe8iG42w_forms_rel_min=if(tmp_qTe8iG42w_forms_rel_min_tmp<2,1,if(tmp_qTe8iG42w_forms_rel_min_tmp==2||tmp_qTe8iG42w_forms_rel_min_tmp==3,if((isnull(tmp_qTe8iG42w_forms_rel_min_tmp[0,-1])&&isnull(tmp_qTe8iG42w_forms_rel_min_tmp[-1,-1])&&isnull(tmp_qTe8iG42w_forms_rel_min_tmp[-1,0])&&isnull(tmp_qTe8iG42w_forms_rel_min_tmp[-1,-1])&&isnull(tmp_qTe8iG42w_forms_rel_min_tmp[0,1]))),1,null()))"

    gscript.run_command('r.mapcalc', quiet=True, expression=mc_expression1)
    gscript.run_command('r.mapcalc', quiet=True, expression=mc_expression2)

    gscript.run_command(
        "r.thin",
        overwrite=True,
        quiet=True,
        input="{}_forms_rel_min".format(tempname),
        output="{}_forms_rel_thin".format(tempname),
    )


    gscript.run_command(
        "r.to.vect",
        overwrite=True,
        quiet=True,
        input="{0}_forms_rel_thin".format(tempname),
        output="{0}_streams_rel".format(tempname),
        type="line",
    )


def create_indices(table, columns, driver="sqlite"):
    """Create indices for columns of a table. Needs to be extended to work for PG!
    :param table: The name of the table where indices are created...
    :type table: str
    :param columns: A list of columns that should be indexed...
    :type columns: list
    :param driver: Name of the current DB driver...
    :type driver: str
    """
    if driver == 'sqlite' or driver == 'pg':
        # PG uses btree by default
        sql_string = "CREATE INDEX {0}_{1}_idx ON {0} ({1});"
    else:
        gscript.fatal(_("Sorry, database driver {} is currently not supported".format(driver)))

    for col in columns:
        gscript.run_command(
            "db.execute",
            quiet=True,
            sql=sql_string.format(
            table, col
        ),
    )


# def classify_network(Graph):
#     """Identify core components in channel network
#     :param Graph: The Graph object imported from igraph...
#     :type igraph-object: str
#     """
#     output = "{0}_streams_rel_points".format(tempname)
#     table = "{}_{}".format(output, "2")
#
#     ## Read Network data from vector map
#     gscript.run_command(
#         "v.net",
#         flags="c",
#         input="{0}_streams_rel".format(tempname),
#         output=output,
#         operation="nodes",
#         node_layer="2",
#         quiet=True,
#         overwrite=True,
#     )
#
#     # Data has to be parsed or written to file as StringIO objects are not supported by igraph
#     # https://github.com/igraph/python-igraph/issues/8
#     net = gscript.read_command(
#         "v.net",
#         input=output,
#         points=output,
#         node_layer="2",
#         operation="report",
#         quiet=True,
#     ).split("\n")
#
#     # Parse network data and extract vertices, edges and edge names
#     edges, vertices, edge_cat = [], [], []
#     for l in net:
#         if l != "":
#             # Names for edges and vertices have to be of type string
#             # Names (cat) for edges
#             edge_cat.append(l.split(" ")[0])
#
#             # From- and to-vertices for edges
#             edges.append((l.split(" ")[1], l.split(" ")[2]))
#
#             # Names (cat) for from-vertices
#             vertices.append(l.split(" ")[1])
#
#             # Names (cat) for to-vertices
#             vertices.append(l.split(" ")[2])
#
#     # Create Graph object
#     g = Graph().as_directed()
#
#     # Add vertices with names
#     vertices.sort()
#     vertices = set(vertices)
#     g.add_vertices(list(vertices))
#
#     # Add vertices with names
#     g.add_edges(edges)
#
#     g.es["cat"] = edge_cat
#
#     # Delete loops
#     g.delete_edges(g.is_loop())
#
#     gscript.verbose(_("Computing neighborhood..."))
#
#     # Compute number of vertices that can be reached from each vertex
#     # Indicates upstream or downstream position of a node
#     g.vs["nbh"] = g.neighborhood_size(mode="all", order=5)
#     g.vs["cl"] = g.as_undirected().clusters().membership
#
#     # Compute incoming degree centrality
#     # sources have incoming degree centrality of 0
#     g.vs["indegree"] = g.degree(mode="in")
#
#     # Compute outgoing degree centrality
#     # outlets have outgoing degree centrality of 0
#     g.vs["outdegree"] = g.degree(mode="out")
#
#     g.vs["uddegree"] = g.degree(mode="all")
#
#     g.vs["betweenness"] = g.betweenness(directed=False)
#
#     line_attrs = []
#     for edge in g.es:
#         line_attrs.append(
#             [min(g.vs[edge.source]["nbh"], g.vs[edge.target]["nbh"]), int(edge["cat"])]
#         )
#     # Compute core edges
#     # g.vs.select(outdegree_le=1, indegree_le=1] #== 1 or  g.degree(type="out")
#
#     gscript.verbose(_("Writing result to table..."))
#
#     # Get Attributes
#     attrs = []
#     for n in g.vs:
#         attrs.append(
#             (
#                 int(n["name"]),
#                 int(n["nbh"]),
#                 int(n["cl"]),
#                 int(n["indegree"]),
#                 int(n["outdegree"]),
#                 int(n["uddegree"]),
#                 int(n["betweenness"]),
#             )
#         )
#
#     # Write results back to attribute table
#     # Note: Backend depenent! For a more general solution this has to be handled
#     conn = sqlite3.connect(get_path(gscript.db.db_connection()["database"]))
#     cur = conn.cursor()
#     cur.execute("DROP TABLE IF EXISTS {}".format(table))
#
#     # Create temporary table
#     cur.execute(
#         """CREATE TABLE {}
#                  (cat integer, neighborhood integer,
#                   cluster integer, indegree integer,
#                   outdegree integer, uddegree integer,
#                   betweenness integer)""".format(
#             table
#         )
#     )
#     conn.commit()
#
#     cur.execute("ALTER TABLE {} ADD COLUMN neighborhood integer;".format(output))
#
#     # Insert data into temporary table
#     cur.executemany("INSERT INTO {} VALUES (?,?,?,?,?,?,?)".format(table), attrs)
#
#     cur.executemany(
#         "UPDATE {} SET neighborhood = ? WHERE cat = ?".format(output), line_attrs
#     )
#
#     # Save (commit) the changes
#     conn.commit()
#     # We can also close the connection if we are done with it.
#     # Just be sure any changes have been committed or they will be lost.
#     conn.close()
#
#     # Connect table to output node layer
#     gscript.run_command("v.db.connect", quiet=True, map=output, table=table, layer="2", flags="o")
#     # Join temporary table to output
#     # gscript.run_command('v.db.join', map=output, layer=node_layer,
#     #                    column='cat', other_table=tmpTable,
#     #                    other_column='cat', quiet=True)
#
#     # Remove temporary table
#     # c = conn.cursor()
#     # c.execute('DROP TABLE IF EXISTS {}'.format(tmpTable))
#     # conn.commit()
#
#     g2 = deepcopy(g)
#     g2.delete_vertices(g.vs.select(uddegree_le=1))
#
#     gscript.run_command("v.db.addcolumn", quiet=True, map=output, column="core integer")
#     gscript.run_command("v.db.update", quiet=True, map=output, column="core", value=0)
#
#     # g.es['betweenness'] = g.edge_betweenness(directed=False, weights=None)
#
#     conn = sqlite3.connect(get_path(gscript.db.db_connection()["database"]))
#     cur = conn.cursor()
#     cur.execute(
#         "UPDATE {} SET core = 1 WHERE cat IN ({});".format(
#             output, ",".join(g2.es["cat"])
#         )
#     )
#     conn.commit()
#     conn.close()
#
#     return table
#
#     sample_raster_values("{0}_streams_rel_points".format(tempname),
#                          "2",
#                          [(elevation, "altitude"),
#                          ("{}_forms_neg_clump".format(tempname), "clump"),
#                          ("{}_barelevdiff".format(tempname), "barelevdiff"),
#                          ("{}_barrier_dist".format(tempname), "bardist")])
#
#     channel_where = "cat IN (SELECT cat FROM {}_streams_rel_points WHERE core = 1 OR length > 10)".format(
#         tempname
#     )
#
#     gscript.run_command(
#         "v.to.db",
#         quiet=True,
#         map="{0}_streams_rel_points".format(tempname),
#         type="line",
#         option="length",
#         columns="length",
#         units="meters",
#         layer="1",
#     )
#
#     # gscript.run_command('db.execute', sql="DROP INDEX streams_rel_points_idx;")
#     create_indices("{0}_streams_rel_points".format(tempname),
#                    ['core'],
#                    db_driver)
#
#     # gscript.run_command('v.extract', overwrite=True, quiet=True, input='streams_rel_points', layer='2',
#     #                    output='streams_rel_pour_points',
#     #                    where=pour_where)
#
#     gscript.run_command(
#         "v.extract",
#         overwrite=True,
#         quiet=True,
#         input="{0}_streams_rel_points".format(tempname),
#         layer="1",
#         output="{0}_streams_rel_channels".format(tempname),
#         where=channel_where,
#     )
#


def extract_pour_points(min_sink_size, elevation, z_tolerance, max_outer_dist, streams):
    """Extract pour points within filled sinks close to barriers"""
    ################################################################################
    # Start pour point identification

    gscript.run_command(
        "r.mapcalc",
        overwrite=True,
        quiet=True,
        expression="{0}_sinks=if({0}_floodlevel,if({0}_forms_rel_diff,1,null()), null())".format(
            tempname
        ),
    )

    # gscript.write_command('r.reclass', overwrite=True, quiet=True,
    #                       input='{}_barrier_dist'.format(tempname),
    #                       output='MASK',
    #                       rules="-",
    #                       stdin="""0 thru {} = NULL
    # * = 1""".format(rcw))

    gscript.run_command(
        "r.clump",
        flags="d",
        overwrite=True,
        quiet=True,
        input="{}_sinks".format(tempname),
        output="{}_sinks_clump".format(tempname),
        minsize=0,
    )

    rc_rules = BytesIO(
        gscript.read_command(
            "r.stats", quiet=True, flags="nc", input="{}_sinks_clump".format(tempname)
        ).encode("UTF8")
    )
    np_arr = np.genfromtxt(rc_rules, dtype=[("cat", "i8"), ("cells", "i8")])
    rel_cats = "\n".join(
        map(str, np_arr["cat"][np.where(np_arr["cells"] > min_sink_size)].tolist())
    )
    rel_cats += "\n"

    if streams:

        gscript.write_command(
            "r.reclass",
            overwrite=True,
            quiet=True,
            input="{}_sinks_clump".format(tempname),
            output="{}_sinks_clump_sizefilter".format(tempname),
            rules="-",
            stdin="{}\n* = 0".format(rel_cats.replace("\n", " = 1\n")),
        )
        gscript.run_command(
            "r.mapcalc",
            overwrite=True,
            quiet=True,
            expression="MASK=if({0}_sinks_clump_sizefilter==1||{0}_stream_dist<({1}/6.0),1,null())".format(tempname, max_outer_dist))
    else:
        gscript.write_command(
            "r.reclass",
            overwrite=True,
            quiet=True,
            input="{}_sinks_clump".format(tempname),
            output="MASK",
            rules="-",
            stdin=rel_cats.replace("\n", " = 1\n"),
        )

    # gscript.write_command('r.reclass', overwrite=True, quiet=True,
    #                     input='{0}_forms_rel'.format(tempname),
    #                     output='MASK',
    #                     rules='-',
    #                     stdin=gscript.read_command('r.stats', flags='nc', separator='=',
    #                                          input='{0}_forms_rel'.format(tempname)))

    # Get pour point as minimum elevation in channel land form (currently not used)
    gscript.run_command(
        "r.stats.zonal",
        overwrite=True,
        quiet=True,
        base="{0}_forms_rel".format(tempname),
        cover=elevation,
        method="min",
        output="{}_sinks_clump_minaltitude".format(tempname),
    )

    gscript.run_command(
        "r.mapcalc",
        overwrite=True,
        quiet=True,
        expression="{0}_sinks_pour_points=if({0}_sinks_clump_minaltitude=={1},{0}_sinks_clump,null())".format(
            tempname, elevation
        ),
    )

    gscript.run_command(
        "r.to.vect",
        overwrite=True,
        quiet=True,
        input="{}_sinks_pour_points".format(tempname),
        output="{}_sinks_pour_points".format(tempname),
        type="point",
    )

    # gscript.run_command('r.neighbors', quiet=True, overwrite=True,
    #                     input=elevation,
    #                     output='{}_tpi_{}_pre'.format(elevation.split('@')[0], 21),
    #                     method='average')
    # gscript.run_command('r.mapcalc', overwrite=True, quiet=True,
    #                     expression="{0}_TPI_{1}={0}_tpi_{1}_pre-{2}".format(
    #                         elevation.split('@')[0], 21, elevation))

    gscript.run_command(
        "v.what.rast",
        quiet=True,
        map="{}_sinks_pour_points".format(tempname),
        raster="{}_barelevdiff".format(tempname),
        column="barelevdiff",
    )
    gscript.run_command(
        "v.what.rast",
        quiet=True,
        map="{}_sinks_pour_points".format(tempname),
        raster="{}_barrier_dist".format(tempname),
        column="bardist",
    )

    # gscript.run_command('v.what.rast', map='{}_sinks_pour_points'.format(tempname),
    #                     raster=elevation, column='altitude')
    # gscript.run_command('v.what.rast', map='{}_sinks_pour_points'.format(tempname),
    #                     raster='{}_forms_neg_clump'.format(tempname), column='clump')
    # gscript.run_command('v.what.rast', map='{}_sinks_pour_points'.format(tempname),
    #                     raster='{}_sinks_clump_avgbardist'.format(tempname),
    #                     column='avgbardist')

    # where = "barelevdiff > -{} OR bardist > {} OR bardist > avgbardist".format(z_tolerance, max_outer_dist)
    where = "barelevdiff > -{0} OR bardist > {1}".format(z_tolerance, max_outer_dist)
    where_extract = "barelevdiff <= -{0} OR bardist <= {1}".format(
        z_tolerance, max_outer_dist
    )
    gscript.run_command(
        "v.extract",
        quiet=True,
        overwrite=True,
        input="{}_sinks_pour_points".format(tempname),
        output="{}_sinks_pour_points_fin".format(tempname),
        where=where_extract,
    )

    # REMOVE
    # gscript.run_command(
    #     "v.edit",
    #     quiet=True,
    #     map="{}_sinks_pour_points".format(tempname),
    #     type="point",
    #     tool="delete",
    #     where=where,
    # )

    gscript.run_command("r.mask", overwrite=True, quiet=True, flags="r")

def find_connections(max_inner_dist, waterbodies):
    """Connect sink pour points with surrounding structures"""
    gscript.run_command(
        "v.distance",
        overwrite=True,
        quiet=True,
        flags="a",
        from_="{}_sinks_pour_points_fin".format(tempname),
        from_layer="1",
        to="{}_streams_rel".format(tempname),
        output="{}_streams_con_pre".format(tempname),
        dmin=1,
        dmax=max_inner_dist,
    )

    if waterbodies:
        gscript.run_command(
            "v.distance",
            overwrite=True,
            quiet=True,
            flags='a',
            from_="{}_sinks_pour_points_fin".format(tempname),
            from_layer="1",
            to=waterbodies,
            output="{}_streams_con_pre_wb".format(tempname),
            dmin=1,
            dmax=max_inner_dist,
        )

        max_cat = gscript.read_command("v.category",
            quiet=True,
            input="{}_streams_con_pre".format(tempname),
            option="report",
            flags="g",
            layer=1).split("\n")[0].split(" ")[4]

        gscript.run_command("v.category",
            quiet=True,
            input="{}_streams_con_pre_wb".format(tempname),
            output="{}_streams_con_pre_wb_max_cat".format(tempname),
            option="sum",
            cat=max_cat)

        gscript.run_command(
            "v.patch",
            overwrite=True,
            quiet=True,
            flags="ab",
            input="{}_streams_con_pre_wb_max_cat".format(tempname),
            output="{}_streams_con_pre".format(tempname),
        )

    # gscript.run_command(
    #     "v.info",
    #     flags='t',
    #     map="{}_streams_con_pre".format(tempname))

    gscript.run_command(
        "v.clean",
        overwrite=True,
        quiet=True,
        input="{}_streams_con_pre".format(tempname),
        output="{}_streams_con".format(tempname),
        type="line",
        tool="rmdupl",
        thres="0.00",
    )


def main():
    """Do the main work"""

    elevation = options["elevation"]
    barriers = options["barriers"]
    culverts = options["culverts"]
    streams = options["streams"]
    filled_dem = options["filled_dem"]

    waterbodies = options["waterbodies"]  # "N50_waterbodies"

    # try:
    #     from igraph import Graph
    # except ImportError:
    #     gscript.fatal("Cannot import igraph library. Make sure it is installed.")

    memory = options["memory"]

    max_inner_dist = float(options["max_inner_dist"])
    max_outer_dist = float(options["max_outer_dist"])
    in_channels = options["channels"]
    minsize = int(options["minsize"])
    min_sink_size = int(options["min_sink_size"])
    z_tolerance = float(options["z_tolerance"])

    # Flags
    # s_flag = flags[
    #     "s"
    # ]  # Smooth land channels before vectorization (creates fewer lines)

    # road_construction_width (one side)
    rcw = 1

    db_driver = check_db_driver()

    # Unset mask if present
    unset_mask()
    # Evaluate use of r.fill.dir instead of r.terraflow
    # Ceck if region settings and raster pixel alignment match
    # before launching r.terraflow
    if not filled_dem:
        if not shutil.which('r.hydrodem'):
            gscript.fatal('Cannot find <r.hydrodem>, please install it: \
g.extension operation=add extension=r.hydrodem')
        else:
            filled_dem = "{}_flooded".format(tempname)
            gscript.run_command(
                "r.hydrodem",
                quiet=True,
                flags='af',
                input=elevation,
                memory=memory,
                output=filled_dem,
            )

    # Compute hight of filling
    mc_expression = "{0}_floodlevel=if(({1} - {2}) > {3}, ({1} - {2}), null())".format(
        tempname, filled_dem, elevation, z_tolerance
    )
    gscript.run_command("r.mapcalc", quiet=True, expression=mc_expression)

    ################################################################################
    # Check if MASK exists and save it temporarily

    # Measure elevation difference between terrain and barriers
    gscript.run_command("r.mask", raster=barriers, quiet=True)
    # Might be OK to drop r.neighbors
    gscript.run_command(
        "r.neighbors",
        input=elevation,
        quiet=True,
        output="{}_barrier_local_height".format(tempname),
        method="average",
        size=5,
    )
    gscript.run_command(
        "r.grow.distance",
        quiet=True,
        input="{}_barrier_local_height".format(tempname),
        distance="{}_barrier_dist".format(tempname),
        value="{}_barrier_height".format(tempname),
    )
    gscript.run_command("r.mask", flags="r", quiet=True)
    mc_expression = "{0}_barelevdiff={1}-{0}_barrier_height".format(tempname, elevation)
    gscript.run_command("r.mapcalc", expression=mc_expression, quiet=True)

    if streams:
        gscript.run_command(
            "r.grow.distance",
            quiet=True,
            input=streams,
            distance="{}_stream_dist".format(tempname))
        gscript.verbose("Computing stream distance {}_stream_dist".format(tempname))

    ############################################################################
    extract_terrain_channels(in_channels, elevation, rcw, minsize, max_outer_dist, streams)
    #table = classify_network(Graph)

    ################################################################################
    # gscript.run_command('v.db.addtable', map='{0}_streams_rel'.format(tempname), layer='2')

    extract_pour_points(min_sink_size, elevation, z_tolerance, max_outer_dist, streams)

    # v.edit map=streams_rel type=line tool=delete where="length<5"
    find_connections(max_inner_dist, waterbodies)

    gscript.run_command(
        "v.category", flags="g",
        overwrite=True,
        option="report",
        input="{}_streams_con".format(tempname))

    print("before")
    gscript.run_command(
        "v.to.points",
        overwrite=True,
        quiet=True,
        input="{}_streams_con".format(tempname),
        output="{}_streams_con_points".format(tempname),
        use="node",
        type="line",
        layer="1",
    )
    print("after")

    gscript.run_command(
        "r.neighbors",
        quiet=True,
        input=elevation,
        size=5,
        output="{}_altitude_local_min".format(tempname),
        method="minimum",
    )

    sample_raster_values("{}_streams_con_points".format(tempname),
                         "2",
                         [("{}_altitude_local_min".format(tempname), "altitude"),
                          ("{}_forms_neg_clump".format(tempname), "clump")])

    # gscript.run_command('db.execute', sql="DROP INDEX streams_rel_points_idx;")
    create_indices("{0}_streams_con_points_2".format(tempname),
                   ['lcat', 'along', 'clump'],
                   db_driver)

    if waterbodies:
        gscript.run_command(
        "db.execute",
        quiet=True,
        sql="UPDATE {0}_streams_con_points_2 SET clump = -1 WHERE clump IS NULL AND along > 0;".format(
            tempname
            )
        )

    tmp_table = """CREATE TABLE {0}_streams_con AS SELECT * FROM (
    SELECT *\
    , CAST(rank() OVER(PARTITION BY from_clump ORDER BY length ASC) AS integer) AS from_clump_length_rank
    , CAST(rank() OVER(PARTITION BY from_clump ORDER BY (from_altitude-to_altitude)/length DESC) AS integer) AS from_clump_slope_rank
    , CAST(min(length) OVER(PARTITION BY from_clump) AS integer) AS from_clump_min_length \
    FROM (\
    SELECT *\
    , CAST(rank() OVER(PARTITION BY from_clump, to_clump ORDER BY length ASC) AS integer) AS length_rank \
    , CAST(rank() OVER(PARTITION BY from_clump, to_clump ORDER BY (from_altitude-to_altitude)/length DESC) AS integer) AS slope_rank \
    FROM (\
    SELECT cat, from_clump, to_clump, from_altitude, to_altitude, length FROM (\
    SELECT lcat AS cat, along AS length, clump AS to_clump, altitude AS to_altitude FROM {0}_streams_con_points_2 WHERE along > 0 AND along < {1}\
    ) AS a NATURAL INNER JOIN (\
    SELECT lcat AS cat, clump AS from_clump, altitude AS from_altitude FROM {0}_streams_con_points_2 WHERE along = 0\
    ) AS b WHERE to_clump != from_clump AND from_altitude > to_altitude - {2}\
    ) AS x ) AS y WHERE length_rank = 1 OR slope_rank = 1\
    ) AS z """.format( # WHERE from_clump_length_rank <= 2 - {2}
        tempname, max_outer_dist, z_tolerance
    )
    #  AND bardist < {1} -- maybe needs to be added back to selection
    gscript.run_command(
        "db.execute",
        quiet=True,
        sql=tmp_table
        )

    create_indices("{0}_streams_con".format(tempname),
                   ['cat'],
                   db_driver)

    gscript.run_command(
        "v.db.connect",
        quiet=True,
        map="{}_streams_con".format(tempname),
        table="{}_streams_con".format(tempname),
        )

    gscript.run_command(
        "v.extract",
        quiet=True,
        input="{}_streams_con".format(tempname),
        layer="1",
        output="{}_streams_con_rel".format(tempname),
        where="cat IS NOT NULL" # "cat IN {}".format(where.replace("\n", "")),
    )

    gscript.run_command(
        "v.extract",
        quiet=True,
        input="{}_streams_con_rel".format(tempname),
        layer="1",
        output="{}_streams_con_rel2".format(tempname),
        where="length <= from_clump_min_length * 1.5",
    )

    gscript.run_command(
        "v.to.points",
        overwrite=True,
        quiet=True,
        input="{}_streams_con_rel2".format(tempname),
        output="{}_streams_con_points".format(tempname),
        use="node",
        type="line",
        layer="1",
    )

    sample_raster_values("{}_streams_con_points".format(tempname),
                         "2",
                         [("{}_floodlevel".format(tempname), "impoundment_depth"),
                          ("{}_sinks_clump_sizefilter".format(tempname), "impoundment_size"),
                          ("{}_forms_neg_clump_size".format(tempname), "to_clump_size")])

    # gscript.run_command('db.execute', sql="DROP INDEX streams_rel_points_idx;")
    create_indices("{0}_streams_con_points_2".format(tempname),
                   ['lcat', 'along'],
                   db_driver)

    sql_cmds = ["ALTER TABLE {0}_streams_con_rel2 ADD COLUMN impoundment_depth double precision;".format(tempname),
                "ALTER TABLE {0}_streams_con_rel2 ADD COLUMN impoundment_size integer;".format(tempname),
                "ALTER TABLE {0}_streams_con_rel2 ADD COLUMN to_clump_size integer;".format(tempname),
                "UPDATE {0}_streams_con_rel2 SET impoundment_size = (SELECT impoundment_size FROM (SELECT lcat, impoundment_size FROM {0}_streams_con_points_2 WHERE along = 0) AS a WHERE a.lcat = {0}_streams_con_rel2.cat)".format(tempname),
                "UPDATE {0}_streams_con_rel2 SET impoundment_depth = (SELECT impoundment_depth FROM (SELECT lcat, impoundment_depth FROM {0}_streams_con_points_2 WHERE along = 0) AS a WHERE a.lcat = {0}_streams_con_rel2.cat)".format(tempname),
                "UPDATE {0}_streams_con_rel2 SET to_clump_size = (SELECT to_clump_size FROM (SELECT lcat, to_clump_size FROM {0}_streams_con_points_2 WHERE along > 0) AS a WHERE a.lcat = {0}_streams_con_rel2.cat)".format(tempname),
                "ALTER TABLE {0}_streams_con_rel2 ADD COLUMN impact double precision;".format(tempname),
                "ALTER TABLE {0}_streams_con_rel2 ADD COLUMN from_clump_impact_rank double precision;".format(tempname),
                "ALTER TABLE {0}_streams_con_rel2 ADD COLUMN overshoot double precision;".format(tempname),
                "ALTER TABLE {0}_streams_con_rel2 ADD COLUMN from_clump_overshoot_rank double precision;".format(tempname),
                "ALTER TABLE {0}_streams_con_rel2 ADD COLUMN barrier_distance_min_rank integer;".format(tempname),
                "ALTER TABLE {0}_streams_con_rel2 ADD COLUMN barrier_distance_avg_rank integer;".format(tempname),
                "UPDATE {0}_streams_con_rel2 SET impact = ((from_altitude + to_altitude) / 2 - altitude_average) * length,\
                                                 overshoot = altitude_minimum - min(from_altitude, to_altitude)".format(tempname),
                "UPDATE {0}_streams_con_rel2 SET from_clump_impact_rank = (SELECT smr FROM (SELECT cat AS lcat, rank() OVER(PARTITION BY from_clump ORDER BY abs(impact) ASC) AS smr FROM {0}_streams_con_rel2) AS a WHERE a.lcat = {0}_streams_con_rel2.cat)".format(tempname),
                "UPDATE {0}_streams_con_rel2 SET from_clump_overshoot_rank = (SELECT smr FROM (SELECT cat AS lcat, rank() OVER(PARTITION BY from_clump ORDER BY overshoot DESC) AS smr FROM {0}_streams_con_rel2) AS a WHERE a.lcat = {0}_streams_con_rel2.cat)".format(tempname)
                ]

    where = "(barrier_distance_maximum < {0}) OR barrier_distance_maximum IS NULL".format(
    max_outer_dist, max_outer_dist / 2.0
    )


    stats_rasters = "{}_barrier_dist,{}".format(tempname, elevation)
    stats_prefix = "barrier_distance,altitude"
    if streams:
        stats_rasters += ",{}_stream_dist".format(tempname)
        stats_prefix += ",stream_distance"

        sql_cmds.append("ALTER TABLE {0}_streams_con_rel2 ADD COLUMN stream_distance_max_rank integer;".format(tempname))
        sql_cmds.append("ALTER TABLE {0}_streams_con_rel2 ADD COLUMN stream_distance_avg_rank integer;".format(tempname))
        sql_cmds.append("UPDATE {0}_streams_con_rel2 SET stream_distance_max_rank = (SELECT smr FROM (SELECT cat AS lcat, rank() OVER(PARTITION BY from_clump ORDER BY stream_distance_maximum ASC) AS smr FROM {0}_streams_con_rel2) AS a WHERE a.lcat = {0}_streams_con_rel2.cat)".format(tempname))
        sql_cmds.append("UPDATE {0}_streams_con_rel2 SET stream_distance_avg_rank = (SELECT smr FROM (SELECT cat AS lcat, rank() OVER(PARTITION BY from_clump ORDER BY stream_distance_average ASC) AS smr FROM {0}_streams_con_rel2) AS a WHERE a.lcat = {0}_streams_con_rel2.cat)".format(tempname))

        where = "(stream_distance_max_rank = 1 OR \
                  from_clump_slope_rank = 1 OR \
                  from_clump_length_rank = 1 OR \
                  from_clump_impact_rank = 1 OR \
                  from_clump_overshoot_rank = 1) OR ({})".format(where)

    sql_cmds.append("ALTER TABLE {0}_streams_con_rel2 ADD COLUMN from_clump_rank_sum integer;".format(tempname))
    sql_cmds.append("UPDATE {0}_streams_con_rel2 SET from_clump_rank_sum = ifnull(from_clump_length_rank, 1) + \
                                                                           ifnull(from_clump_slope_rank, 1) + \
                                                                           ifnull(from_clump_impact_rank, 1) + \
                                                                           ifnull(from_clump_overshoot_rank, 1) + \
                                                                           ifnull(stream_distance_avg_rank, 1) + \
                                                                           ifnull(stream_distance_max_rank, 1) + \
                                                                           ifnull(barrier_distance_avg_rank, 1) + \
                                                                           ifnull(barrier_distance_min_rank, 1)".format(tempname))
    sql_cmds.append("UPDATE {0}_streams_con_rel2 SET barrier_distance_min_rank = (SELECT bdm FROM (SELECT cat AS lcat, rank() OVER(PARTITION BY from_clump ORDER BY barrier_distance_minimum ASC) AS bdm FROM {0}_streams_con_rel2) AS a WHERE a.lcat = {0}_streams_con_rel2.cat)".format(tempname))
    sql_cmds.append("UPDATE {0}_streams_con_rel2 SET barrier_distance_avg_rank = (SELECT bda FROM (SELECT cat AS lcat, rank() OVER(PARTITION BY from_clump ORDER BY barrier_distance_average ASC) AS bda FROM {0}_streams_con_rel2) AS a WHERE a.lcat = {0}_streams_con_rel2.cat)".format(tempname))
    sql_cmds.append("ALTER TABLE {0}_streams_con_rel2 ADD COLUMN culvert_rank integer;".format(tempname))
    sql_cmds.append("UPDATE {0}_streams_con_rel2 SET culvert_rank = (SELECT cr FROM (SELECT cat AS lcat, rank() OVER(PARTITION BY from_clump ORDER BY from_clump_rank_sum ASC) AS cr FROM {0}_streams_con_rel2) AS a WHERE a.lcat = {0}_streams_con_rel2.cat)".format(tempname))


    full_stats = gscript.read_command(
        "v.rast.stats",
        quiet=True,
        flags="c",
        map="{}_streams_con_rel2".format(tempname),
        raster=stats_rasters,
        method="number,maximum,minimum,average",
        column_prefix=stats_prefix,
        stderr=subprocess.STDOUT
    )
    # Replace the following with while gscript.db_select(altitude_number IS NULL) ?
    if "WARNING: Not all vector categories converted to raster" in full_stats:
        full_stats = gscript.read_command(
            "v.rast.stats",
            quiet=True,
            flags="c",
            map="{}_streams_con_rel2".format(tempname),
            raster=stats_rasters,
            method="maximum,minimum,average",
            column_prefix=stats_prefix,
            where="altitude_number IS NULL",
            stderr=subprocess.STDOUT
        )


    for sql in sql_cmds:
        gscript.run_command(
            "db.execute",
            quiet=True,
            sql=sql
        )

    gscript.run_command(
        "v.extract",
        overwrite=True,
        quiet=True,
        input="{}_streams_con_rel2".format(tempname),
        layer="1",
        output=culverts,
        where=where,
    )

    # Write history to output
    gscript.run_command(
        "v.support", map=culverts, cmdhist=os.environ["CMDLINE"], flags="h"
    )
    # vector_history(culverts)


if __name__ == "__main__":
    options, flags = gscript.parser()
    atexit.register(cleanup)
    main()
